""" The actual game engine. """

from __future__ import annotations

from os import path

from colorama import init

from lord.base.character import Player
from lord.base.scene import Scene, LeaveGame
from lord.design.scenes import Town
from lord.tools.input import input_key, input_string
from lord.tools.utilities import character_filename, load_player_from_disk

init()  # colorama's init


class Game:
    def __init__(self, starting_scene: Scene) -> None:
        """Launch and play a new game"""
        assert starting_scene
        self.scene: Scene = starting_scene
        self.play()

    def must_quit(self) -> bool:
        """Indicate if we should quit."""
        return isinstance(self.scene, LeaveGame)  # LeaveGame is a magic scene that tells the game we are leaving.

    def play(self) -> None:
        """Main game loop."""
        while not self.must_quit():  # "while" is called a loop!
            # draw the screen for the player.
            self.scene.draw()

            # We will loop until the player enters a correct choice.
            new_scene = None
            while not new_scene:
                # ask+wait for the player to enter a choice and record his answer
                player_choice = input_key()
                # tell our scene what the player selected. The new scene is saved in the variable.
                # If the player made a bad choice, new_scene will still be None...
                new_scene = self.scene.play_action(player_choice)

            # the scene.play_action returned a new scene, so use it for the next loop.
            self.scene = new_scene


if __name__ == '__main__':
    """Starts the game loop."""
    print("\n\nWelcome to LORD!\n")

    if path.exists(character_filename):
        print("Loading player data...")
        player1 = load_player_from_disk()
        print(f"Player loaded successfully! Welcome back, {player1.name}!")
    else:
        print("Enter your name!")
        name = input_string()
        player1 = Player(name=name)

    # make sure the player has the fireball and ice missile spells.
    from design.spells import Fireball, IceMissile

    player1.spells.append(Fireball())
    player1.spells.append(IceMissile())
    game = Game(Town(player1))
