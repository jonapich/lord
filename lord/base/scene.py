"""

Base building blocks to create new game scenes.


"""

from __future__ import annotations

import random
from abc import abstractmethod
from typing import Optional, ClassVar, List, Type, Union

from colorama import Fore
from colorama.ansi import clear_screen

from design.spells import Spell
from lord.base.character import Character, Monster
from lord.tools.input import wait_for_key
from tools.input import input_key


class Scene:
    """A place in the game."""

    def __init__(self, player: Character) -> None:
        assert player
        self.player: Character = player

    def draw(self) -> None:
        """Draw the main game screen"""
        clear_screen()
        print()
        print(Fore.MAGENTA + "   ***   " * 5 + Fore.RESET)
        print()
        self.print_scene()
        print()
        print(Fore.BLUE)
        print(self.player)
        print(Fore.YELLOW)
        self.print_choices()
        print(Fore.RESET)

    @abstractmethod
    def print_scene(self) -> None:
        """Print the text for this scene."""

    @abstractmethod
    def print_choices(self) -> None:
        """Print the choices for this scene."""

    @abstractmethod
    def play_action(self, action: str) -> Optional[Scene]:
        """Play the action. Return the next scene to use (it can be the same!), or None if the action is wrong."""


class AttackScene(Scene):
    """Base logic for attack scenes."""
    MONSTERS: ClassVar[List[Type[Monster]]]

    def __init__(self, player: Character, return_scene: Union[Scene, Type[Scene]]) -> None:
        # summon and remember a monster.
        super().__init__(player)
        assert self.MONSTERS, "A monster list is missing!"
        monster_kind = random.choice(self.MONSTERS)
        self.monster = monster_kind()
        self.player = player
        self.return_scene: Union[Scene, Type[Scene]] = return_scene

    def print_scene(self) -> None:
        print("A monster stands before you.")

    def print_choices(self) -> None:
        print(self.monster)
        print("a - attack")
        if self.player.available_spells:
            print("c - cast spell")
        print("r - run away")

    def end_battle(self) -> Scene:
        """End the battle and return the next scene."""
        wait_for_key()
        return self.return_scene if isinstance(self.return_scene, Scene) else self.return_scene()

    def play_action(self, action: str) -> Optional[Scene]:
        monster = self.monster
        spell: Optional[Spell] = None

        if action == 'c' and self.player.available_spells:
            print("Which spell?")
            available_spells = self.player.available_spells
            for i, player_spell in enumerate(available_spells):
                print(f"{i}. {player_spell.name}")
            spell_choice = input_key()
            if spell_choice.isnumeric():
                spell = available_spells[int(spell_choice)]
                action = 'a'  # spell is ready, switch to attack!
            else:
                return None  # that's an error.

        if action == 'a':
            while True:
                if self.player.stunned:
                    # the player is stunned and must skip this turn
                    print(Fore.RED + "You are stunned!")
                    wait_for_key()
                    self.player.stunned -= 1
                else:
                    # the player attacks the monster.
                    if spell:
                        spell.cast(self.player, monster)
                    else:
                        self.player.attack(monster)

                if monster.hit_points <= 0:
                    print(Fore.RED + "You killed the monster!" + Fore.RESET)
                    self.player.reward(monster.gold)
                    return self.end_battle()

                if monster.stunned:
                    # The monster is stunned and must skip this turn.
                    monster.stunned -= 1
                    if monster.stunned:
                        print(Fore.RED + monster.name + " is stunned!")
                    else:
                        print(Fore.GREEN + monster.name + " woke up!")
                else:
                    # the monster was not killed and attacks the player!
                    monster.attack(self.player)

                if self.player.hit_points <= 0:
                    print(Fore.RED + "Uh oh, the monster killed you :(" + Fore.RESET)
                    self.end_battle()  # we ignore the return value because we want LeaveGame.
                    return LeaveGame(self.player)

                if self.player.stunned:
                    # the player is stunned, so we force another attack
                    continue  # start the loop again
                else:
                    break  # get out of the loop and give control to the player.

            # everyone is alive; the battle is not over!
            return self

        elif action == 'r':
            # todo: Run away game mechanic (choose a path and maybe it works or maybe you fall and it fails!)
            print(Fore.RED + "You ran away!" + Fore.RESET)
            return self.end_battle()


class LeaveGame(Scene):  # magic scene! the game leaves instead of playing this scene.
    """The game is over! exit."""

    def print_choices(self) -> None:
        ...  # never called

    def print_scene(self) -> None:
        ...  # never called

    def play_action(self, action: str) -> Optional[Scene]:
        ...  # never called
