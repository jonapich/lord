"""

Character definition.

A character is something in the game that has hit points, can attack, etc.

It could be a monster or a player, or maybe an NPC too.

"""


from __future__ import annotations

import random
from typing import Union, ClassVar, List, Dict, Any, TYPE_CHECKING

from colorama import Fore
from inflection import underscore

if TYPE_CHECKING:
    from lord.design.spells import Spell


class Character:
    """A character, such as a player or monster."""
    name: Union[ClassVar[str], str]
    strength: int
    max_hit_points: int
    max_magic_points: int
    gold: int
    stunned: int = 0  # amount of turns stunned.

    def __init__(self, name: str = None):
        self.name = name or \
                    getattr(self, 'name', None) or \
                    underscore(self.__class__.__name__).replace('_', ' ').title()
        self.hit_points: int = self.max_hit_points  # heal the player
        self.magic_points: int = self.max_magic_points
        self.spells: List[Spell] = []
        assert self.name, "Name should not be empty!"
        assert self.max_hit_points > 0, "Max hit points must be greater than 0!"
        assert self.max_magic_points >= 0, "Max hit points must be equal or greater than 0!"
        assert self.strength >= 0, "Strength should be equal or greater than 0!"
        assert self.gold >= 0, "Gold should be equal or greater than 0!"

    def __str__(self) -> str:
        """Print the current character stats."""
        lines: List[str] = [self.name,
                            f"HP: {self.hit_points}/{self.max_hit_points}",
                            f"ATK: {self.strength}",
                            f"MP: {self.magic_points}/{self.max_magic_points}",
                            f"$ {self.gold}"]
        text = "| " + "  |  ".join(lines) + " |"
        separator = "-" * len(text)

        return '\n'.join((separator, text, separator))

    def heal(self, amount: int = None) -> None:
        """Heal the character by `amount`. If amount is None, heal completely."""
        if amount:
            self.hit_points = min(self.max_hit_points, self.hit_points + amount)
            print(f"{self.name} was healed by {amount} hit points!")
        else:
            self.hit_points = self.max_hit_points
            print(f"{self.name} was healed completely!")

    def heal_magic(self, amount: int = None) -> None:
        """Heal the character by `amount`. If amount is None, heal completely."""
        if amount:
            self.magic_points = min(self.max_magic_points, self.magic_points + amount)
            print(f"{self.name} restored {amount} magic points!")
        else:
            self.magic_points = self.max_magic_points
            print(f"{self.name}'s magic points were restored completely!")

    def hit(self, amount: int) -> int:
        """Reduce the player health by amount. May crit."""
        if random.random() <= 0.05:  # 5% crit chance
            print(f"{Fore.RED}!! CRIT !!{Fore.RESET}")
            amount = round(amount * random.uniform(1.2, 2))  # between 1.2x and 2x damage
        self.hit_points -= amount
        return amount

    def reward(self, gold_reward: int) -> None:
        """Reward the player with gold."""
        self.gold += gold_reward
        print(f"{self.name} is richer by {gold_reward} gold!")

    @property
    def available_spells(self) -> List[Spell]:
        """Return the list of spells the character can currently cast"""
        return [spell for spell in self.spells if self.magic_points >= spell.cost]

    def pay(self, cost: int) -> bool:
        """Take gold away from the player. Return True if successful,
        False if the player doesn't have enough gold."""
        if cost > self.gold:
            input("\nSorry! Not enough gold!!!")
            return False
        else:
            self.gold -= cost
            input(f"\n{cost} gold was removed from {self.name}!")
            return True

    def attack(self, target: Character, amount: int = None) -> None:
        """The target can be a Monster or another Player."""
        amount = target.hit(self.strength if amount is None else amount)
        print(f"{self.name} attacks {target.name} for {amount} damage!")

    @classmethod
    def load(cls, **kwargs) -> Character:
        """Load a character from values."""
        character = cls()
        for stat, value in kwargs.items():
            setattr(character, stat, value)
        return character

    def as_dict(self) -> Dict[str, Any]:
        """Return the character as a dictionary, to save to disk."""
        data: Dict[str, Any] = {}
        for stat in 'name', 'strength', 'max_hit_points', 'hit_points', 'gold':
            data[stat] = getattr(self, stat)
        return data


class Player(Character):
    """The player"""
    max_hit_points = 100
    max_magic_points = 50
    strength = 4
    gold = 100


class Monster(Character):
    """A monster"""
    max_magic_points = 0
    max_hit_points = 5
    strength = 1
    gold = 0
