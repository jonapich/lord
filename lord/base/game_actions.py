""" All possible game actions. """

from lord.base.character import Character


# the fireball is not currently used.
def fireball(target: Character) -> None:
    """ Attacks using a fireball for 20 damage. """
    print("Fireball hits for 20 damage!")
    target.hit(amount=20)


