"""
PyCharm-compatible input mechanism (switch on `Emulate Terminal in Output Console` in your run configuration).

TODO: input_key should receive and display a list of OK options
TODO: handle backspace in input_string (currently prints a weird character box)
"""

from readchar import readchar, readkey
from colorama import Fore

ENTER_KEYS = '\n', '\r'


def _print_prompt(message: str = '') -> None:
    """Print the [*] prompt on screen and an optional message."""
    print(f"\n{Fore.LIGHTBLUE_EX}[{Fore.WHITE}*{Fore.LIGHTBLUE_EX}]{Fore.RESET} {message}", end='')


def input_key() -> str:
    """Get the next key pressed."""
    _print_prompt()
    while True:
        key = readkey()
        if key not in ENTER_KEYS:
            return key


def wait_for_key() -> None:
    """Wait until the next key pressed."""
    _print_prompt("(press any key!) ")
    readkey()


def input_string(limit: int = 64) -> str:
    """Read a sequence of keys pressed."""
    _print_prompt()
    string = ""
    while len(string) <= limit:
        new_character: str = readchar()
        print(new_character, end='')
        if new_character in ENTER_KEYS:
            break
        string += new_character
    return string
