""" Various utilities for the game. """

import json
from os import path

from lord.base.character import Character, Player

DATA_PATH = path.abspath(path.join(path.dirname(path.realpath(__file__)), '..', '..', 'tests', 'data'))
character_filename = path.join(DATA_PATH, 'lord.character')


def load_player_from_disk() -> Character:
    with open(character_filename) as saved_game:
        return Player.load(**json.load(saved_game))


def save_player_to_disk(character: Character) -> None:
    with open(character_filename, 'w') as saved_game:
        json.dump(character.as_dict(), saved_game)
