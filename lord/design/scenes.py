""" The scenes in the game. """

from typing import Optional

from lord.base.scene import Scene, LeaveGame, AttackScene
from lord.design.monsters import MinionsCar, MisterSpike, PrincessSnow
from lord.tools.utilities import save_player_to_disk


# *** TOWN STUFF ***

TOWN_DESCRIPTION = """\
You are in Pumpkin Town. The villagers here are friendly, but they love pumpkin soup too much! 
Some people even say that is the reason why monsters have started invading the town! 

They say someone will soon open an equipment shop... That would sure be useful! 

You can visit the INN for now, or you can go to the forest!"""


class Town(Scene):
    def print_scene(self) -> None:
        print(TOWN_DESCRIPTION)

    def print_choices(self) -> None:
        print("i - Visit the INN")
        print("f - Go to the Forest")
        print("x - Leave game")

    def play_action(self, action: str) -> Optional[Scene]:
        if action == "i":
            return Inn(self.player)
        elif action == "f":
            return CreepyForest(self.player)
        elif action == "x":
            return LeaveGame(self.player)


# *** FOREST STUFF ***

FOREST_DESCRIPTION = """\
You entered a creepy forest... You can hear the sound of squiggly slimy crawling things all around you, ew!

You take your sword and shield, your courage, and you venture into the forest to find some loot..."""


class CreepyForest(Scene):
    def print_scene(self) -> None:
        print(FOREST_DESCRIPTION)

    def print_choices(self) -> None:
        print("x - Go back to Town")
        print("a - Attack monster")

    def play_action(self, action: str) -> Optional[Scene]:
        if action == "x":
            return Town(self.player)
        elif action == "a":
            return CreepyForestAttack(self.player, self)


class CreepyForestAttack(AttackScene):
    MONSTERS = MinionsCar, MisterSpike, PrincessSnow


# *** INN STUFF ***

class Inn(Scene):
    cost: int = 10

    def print_scene(self) -> None:
        print("You are at the Pumpk-INN! You can save your game and recharge your HP for 10 gold!")

    def print_choices(self) -> None:
        print("s - Sleep, save and restore hit points (10 gold)")
        print("t - Go back to town")

    def play_action(self, action: str) -> Optional[Scene]:
        if action == 's':
            if self.player.pay(self.cost):
                self.player.heal()
                self.player.heal_magic()
                save_player_to_disk(self.player)
            return self
        elif action == 't':
            return Town(self.player)
