"""
The monsters in the game are defined here.

If the name is not given, the class name is used so that `MinionsCar` becomes `Minions Car`.

You can create different versions of a monster using "class inheritance":

First, create the normal version of the monster

    class PoorMonster(Monster):
        strength = 5
        gold = 0

Then, "inherit" from PoorMonster and change what you want different.
Inherit means to use (PoorMonster) instead of (Monster):

    class RichMonster(PoorMonster):
        gold = 100

RichMonster will have a strength of 5 because it "inherits" from PoorMonster. But only the rich monster has gold!


TODO: Monster rarity
TODO: Randomize gold
TODO: Loot
"""

from lord.base.character import Monster


class MinionsCar(Monster):
    max_hit_points = 10
    strength = 1
    gold = 80


class MisterSpike(Monster):
    max_hit_points = 15
    strength = 2
    gold = 100


class PrincessSnow(Monster):
    strength = 5
    max_hit_points = 20
    gold = 1


class GoldenPrincessSnow(PrincessSnow):  # <- "Inherit" from PrincessSnow, keep the same stats
    gold = 8000  # <-- but change the gold!


# REMEMBER: When you add new monsters, you must add it to an Attack Scene too!
