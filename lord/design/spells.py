"""

The spells are defined here.

"""
from __future__ import annotations

import random
from abc import abstractmethod
from typing import TYPE_CHECKING

from inflection import underscore

if TYPE_CHECKING:
    from lord.base.character import Character


class Spell:
    # how much magic points does the spell cost
    cost: int = 10
    # how accurate is the spell. 0.8 is for 80%. 1 is 100%.
    accuracy: int = 0.8
    # a description that explains the spell
    description: str

    def __init__(self) -> None:
        self.name = underscore(self.__class__.__name__).replace('_', ' ').title()
        assert self.accuracy <= 1

    def cast(self, caster: Character, *targets: Character) -> None:
        """Cast the spell on targets"""
        # reduce the caster's mp
        caster.magic_points -= self.cost

        # each target has a chance to dodge the spell.
        for target in targets:
            if random.random() <= self.accuracy:
                # reduce the target's hit points
                caster.attack(target, self.calculate_damage(caster, target))
            else:
                print(f"{target.name} dodged the spell!")

    @abstractmethod
    def calculate_damage(self, caster: Character, target: Character) -> int:
        """Return the amount of damage made by the spell."""


class Fireball(Spell):
    cost = 20
    accuracy = 1
    description = "Between 1.5x and 2x damage, always hits."

    def calculate_damage(self, caster: Character, target: Character) -> int:
        return round(caster.strength * random.uniform(1.5, 2))  # between 1.5x and 2x


class IceMissile(Spell):
    cost = 5
    accuracy = 0.5
    description = "50% chance to stun the enemy for 2 turns. Does no damage."

    def calculate_damage(self, caster: Character, target: Character) -> int:
        target.stunned += 2
        return 0  # no damage
